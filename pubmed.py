from metapub import PubMedFetcher
import psycopg2
import time
from typing import List
from wordcloud import WordCloud
from PIL import Image
from pymetamap import MetaMap
from collections import Counter
import sys
from py4j.java_gateway import JavaGateway


def connect_to_database():
    argumentList: list[str] = list(sys.argv)

    host: str = argumentList[1]
    database: str = argumentList[2]
    user: str = argumentList[3]
    password: str = argumentList[4]

    conn = psycopg2.connect(
        host=host,
        database=database,
        user=user,
        password=password)
    return conn


def get_article_list(conn, size: int):
    cur = conn.cursor()
    cur.execute(
        "SELECT COUNT (title) FROM main_table WHERE main_table.title similar to '%("
        "children|kid|child|pediatrics|adolesc|Child|Kid|Pediatric|Adolesc)%' AND main_table.pubmed_id IS NOT NULL; "
    )
    articleSize = cur.fetchall()
    print(str(articleSize[0][0]))
    cur.execute(
        "SELECT (pubmed_id) FROM main_table WHERE main_table.title SIMILAR TO '%(children|kid|child|pediatrics|adolesc|Child|Kid|Pediatric|Adolesc)%' "
        "AND main_table.pubmed_id IS NOT NULL")
    # + str(articleSize[0][0])
    articleidlist: list = cur.fetchall()
    print(str(len(articleidlist)))
    conn.close()
    #print(articleidlist)
    return articleidlist

#children|kid|child|pediatrics|adolesc|Child|Kid|Pediatric|Adolesc
#eld|geriatric|old|50|aged|aging|
#children|kid|child|pediatrics|Children|Kid|Child
#female|women|girl|Female|Women|Girl
# male| man|boy|Male|Man|Boy| men|Men

def copy_to_postgres_table(fullList, conn):
    cur = conn.curson()
    cur.execute(
        "CREATE TABLE full_mesh_list ("
        " mesh_id SMALLINT(20000) NOT NULL, mesh_term VARCHAR(100) NOT NULL, mesh_count SMALLINT(2000) NOT NULL, PRIMARY KEY (mesh_id) );"
    )
    s = ""
    s += "INSERT INTO tbl_users"
    s += "("
    s += "t_name_user"
    s += ") VALUES ("
    s += "%param"
    s += ")"

    cur.execute(s, fullList)


def meta_map(word: str, metamap):
    concepts = metamap.extract_concepts(word)
    conceptListString = "bpoc,medd,mbrt,dsyn,neop,aapp,imft,phsu,orch,enzy,gngm,nnon,rcpt,hcro,orgt,shro,cgab,anab,fndg,sosy"
    conceptList = conceptListString.split(',')
    for f in range(len(concepts[0])):
        for j in range(len(conceptList)):
            splitConceptList = check_split_string(concepts[0][f][5])
            for k in range(len(splitConceptList)):
                if splitConceptList[k] == conceptList[j]:
                    print("meta map success")
                    return True
    print("meta map success but false")
    return False

def get_metamap_from_py4j(gateway, word: str):
    metamapString = gateway.entry_point.checkForValidity(word)
    if ("semantic-types" in metamapString):
        return True
    else:
        return False


def check_split_string(inputString: str):
    splitInput: list[str] = inputString.split(',')
    for i in range(len(splitInput)):
        tempString : str = ""
        for j in range(len(splitInput[i])):
            if splitInput[i][j].isalpha():
                tempString = tempString + splitInput[i][j]
        splitInput[i] = tempString
    return splitInput


def fetch_article(article_IDLIST, article_Counter):
    print("beforefetch")
    print(article_Counter)
    # print(len(article_IDLIST))
    print(article_IDLIST[article_Counter])
    article = fetch.article_by_pmid(article_IDLIST[article_Counter])
    print("afterfetch")
    meshh = article.mesh
    return meshh


def get_word_from_article(mesh: dict, dictList: list, i: int):
    dictWord = mesh[dictList[i]]['descriptor_name'].lower()
    return dictWord



def make_image(KeyWordDict: dict):
    print("makeImage started")
    wc = WordCloud(width=4000, height=2000, background_color="white", max_words=1000)
    print("wordcloudmade")
    wc.generate_from_frequencies(KeyWordDict)

    print("wordcloud generated")
    image = wc.to_image()
    print("image made")
    image.save('/home/sujay-spare-server/Documents/TempPILStuff/pilphototest.jpg', 'JPEG')
    print("image saved")


def compare_to_exclusion_list(exclusion: list, newword: str):
    for word in exclusionList:
        if word in newword:
            return True
    return False


if __name__ == "__main__":
    db_conn = connect_to_database()

    articleIDLIST: list = get_article_list(db_conn, 1000)

    # export NCBI_API_KEY="ab7d33f9f9e7e89d018c85aa5262925d6b08"
    fetch = PubMedFetcher()

    articleCounter: int = 0

    start_time: float = time.time()

    KeyWordDict = Counter()

    #mm = MetaMap.get_instance('/usr/local/metamap/public_mm/bin/metamap20')

    gateway = JavaGateway()

    exclusionList: list = {"human", "animal", "male", "female", "middle", "adult", "mice", "child", "aged",
                           "adolescent", "rna", "antibodies", "genome", "covid-19"}

    while articleCounter < len(articleIDLIST):
        current_time: float = time.time()
        elapsed_time: float = current_time - start_time

        if elapsed_time > .8:
            print("finished iterating in:" + str(elapsed_time))
            print(str(articleCounter))

            try:
                mesh: dict = fetch_article(articleIDLIST, articleCounter)
                dictList: list = list(mesh.keys())
                listLength: int = len(dictList)
                print('success')

                for i in range(int(listLength)):
                    dictWord = get_word_from_article(mesh, dictList, i)
                    dictWordSplit = dictWord.split(",")
                    for k in range(int(len(dictWordSplit))):
                        if not (compare_to_exclusion_list(exclusionList, dictWordSplit[k])):
                            if (get_metamap_from_py4j(gateway, dictWordSplit[k])):
                               # print("done2")
                                KeyWordDict[dictWordSplit[k]] += 1


                start_time = time.time()

            except:
                print("failure")
                start_time = time.time() + 8

            articleCounter += 1
    gateway.shutdown()
    print('alright this is done otherwise the problem is before this')

    make_image(KeyWordDict)
